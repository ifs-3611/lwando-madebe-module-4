// ignore_for_file: prefer_const_constructors, deprecated_member_use

import 'package:flutter/material.dart';
// ignore: unused_import


class MyContacts extends StatefulWidget {
  const MyContacts({Key? key}) : super(key: key);

  @override
  State<MyContacts> createState() => _MyContactsState();
}

class _MyContactsState extends State<MyContacts> {
  


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[100],
      body: Center(
        child: Column(
          children: <Widget> [
            Text("Contact Us",
            style: TextStyle(
              fontSize: 32.0,
              fontWeight: FontWeight.bold,
            ),
            ),
            Text("For all enquiries, message us using the form below"),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32.0),
              child: Column(
                // ignore: prefer_const_literals_to_create_immutables
                children: <Widget>[
                  SizedBox(height: 16.0,),
                  TextField(
                    decoration: InputDecoration(
                      filled: true,
                       fillColor: Colors.white,
                      hintText: "Name",
                      border: InputBorder.none,
                    ),
                  ),
                  SizedBox(height: 8.0,),
                     TextField(
                    decoration: InputDecoration(
                      filled: true,
                       fillColor: Colors.white,
                      hintText: "Email",
                      border: InputBorder.none,
                    ),
                  ),
                  SizedBox(height: 8.0),
                     TextField(
                      maxLines: 7,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      hintText: "Message",
                      border: InputBorder.none,
                    ),
                  ),
                  SizedBox(height: 8.0),
                FlatButton(
                    color: Colors.blueGrey,
                    onPressed: () {}, 
                    child: Text('Submit'),
                    ),
                ],
              ),
            )
          ],
        ),
      ),
      );
    
  }
}
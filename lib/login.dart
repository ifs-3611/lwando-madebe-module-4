

// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, deprecated_member_use


import 'package:app_develop/dashboard.dart';
import 'package:app_develop/register.dart';
import 'package:flutter/material.dart';

class MyLogin extends StatefulWidget {
  const MyLogin({Key? key}) : super(key: key);

  @override
  State<MyLogin> createState() => _MyLoginState();
}

class _MyLoginState extends State<MyLogin> {
  bool isHiddenPassword =true;
  
  get mainAxisAlignment => null;
  
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage( 
          image: AssetImage('images/background.png'), fit: BoxFit.cover)),
       child: Scaffold(
          backgroundColor: Colors.transparent, 
          body: Stack(
            children: [
              // ignore: avoid_unnecessary_containers
              Container(
                
                padding: EdgeInsets.only(left: 30, top: 110),
                child: Text (
                  'Welcome\nPlease login',
                  style: TextStyle(color: Colors.black, fontSize: 30),
              ),
              ),
            Container(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.5, right: 35, left: 35),
              child: Column(
                children: [
                  TextField(
                    decoration: InputDecoration(
                      fillColor: Colors.grey.shade200,
                      filled: true,
                      hintText: 'Username or email',
                      border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(10)
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  TextField(
                    obscureText:  isHiddenPassword,
                    decoration: InputDecoration(
                      fillColor: Colors.grey.shade200,
                      filled: true,
                      hintText: 'Password',
                      suffixIcon: InkWell(
                        onTap: _togglePasswordView,
                        child: Icon (
                          Icons.visibility
                          ),
                      ),
                      border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(10)
                      ),
                    ),
                  ),
                 
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      TextButton(
                        onPressed: () { 
                          Navigator.push(context, MaterialPageRoute(builder: (context) => MyRegister()),);
                        }, 
                        child: Text(
                          'Sign Up', 
                          style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontSize: 12,
                        color: Colors.blueGrey,
                        ),
                        ) )
                    ],
                  ),
                    
                  Row(
                     mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      TextButton(
                        onPressed: () {}, 
                        child: Text(
                          'Lost password?', 
                          style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontSize: 12,
                        color: Colors.blueGrey,
                        ),
                        ) )
                    ],
                  ),

                  SizedBox(
                    height: 20,
                  ),
                  FlatButton(
                    color: Colors.blueGrey,
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => MyDashboard()),);
                    }, 
                    child: Text('Login'),
                    ),
                ],
              )
            )
            ],
          ),
        ),
        ); 
  }
  void _togglePasswordView(){
    if(isHiddenPassword == true){
      isHiddenPassword = false;
      } else {
        isHiddenPassword= true;
      }
      setState(() {});
    }
  }


